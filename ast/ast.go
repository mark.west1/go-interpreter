package ast

import (
	"bytes"
	"strings"

	"gitlab.com/mark.west1/go-interpreter/token"
)

// Node is the most basic functionality of the abstract syntax tree
type Node interface {
	TokenLiteral() string
	String() string
}

// Statement nodes do not produce values
type Statement interface {
	Node
	statementNode()
}

// Expression nodes produce values
type Expression interface {
	Node
	expressionNode()
}

// Program is the root of the AST
type Program struct {
	Statements []Statement
}

// TokenLiteral on *Program makes it the only basic Node
func (p *Program) TokenLiteral() string {
	if len(p.Statements) > 0 {
		return p.Statements[0].TokenLiteral()
	}

	return ""
}

// String fulfills the Node interface for Program
func (p *Program) String() string {
	var out bytes.Buffer

	for _, s := range p.Statements {
		out.WriteString(s.String())
	}

	return out.String()
}

// LetStatement Token.LET
type LetStatement struct {
	Token token.Token
	Name  *Identifier
	Value Expression
}

// statementNode fulfills the Statement interface for ReturnStatement
func (ls *LetStatement) statementNode() {}

// TokenLiteral fulfills Node interface for LetStatement
func (ls *LetStatement) TokenLiteral() string { return ls.Token.Literal }

// String fulfills the Node interface for LetStatement
func (ls *LetStatement) String() string {
	var out bytes.Buffer

	out.WriteString(ls.TokenLiteral() + " ")
	out.WriteString(ls.Name.String())
	out.WriteString(" = ")

	if ls.Value != nil {
		out.WriteString(ls.Value.String())
	}

	out.WriteString(";")

	return out.String()
}

// ReturnStatement Token.RETURN
type ReturnStatement struct {
	Token       token.Token
	ReturnValue Expression
}

// statementNode fulfills Statement interface for ReturnStatement
func (rs *ReturnStatement) statementNode() {}

// TokenLiteral fulfills Node interface for ReturnStatement
func (rs *ReturnStatement) TokenLiteral() string { return rs.Token.Literal }

// String fulfills the Node interface for ReturnStatement
func (rs *ReturnStatement) String() string {
	var out bytes.Buffer

	out.WriteString(rs.TokenLiteral() + " ")

	if rs.ReturnValue != nil {
		out.WriteString(rs.ReturnValue.String())
	}

	out.WriteString(";")

	return out.String()
}

// ExpressionStatement Token.*
type ExpressionStatement struct {
	Token      token.Token
	Expression Expression
}

// statementNode fulfills the Statement interface for ExpressionStatement
func (es *ExpressionStatement) statementNode() {}

// TokenLiteral fulfills the Node interface for ExpressionStatement
func (es *ExpressionStatement) TokenLiteral() string { return es.Token.Literal }

// String fulfills the Node interface for ExpressionStatement
func (es *ExpressionStatement) String() string {
	if es.Expression != nil {
		return es.Expression.String()
	}

	return ""
}

// Identifier Token.IDENT
type Identifier struct {
	Token token.Token
	Value string
}

// expressionNode fulfills Expression interface for Identifier
func (i *Identifier) expressionNode() {}

// TokenLiteral fulfills the Node interface for Identifier
func (i *Identifier) TokenLiteral() string { return i.Token.Literal }

// String fulfills the Node inerface for Identifier
func (i *Identifier) String() string { return i.Value }

// IntegerLiteral Token.INT
type IntegerLiteral struct {
	Token token.Token
	Value int64
}

// expressionNode fulfills the Expression interface for IntegerLiteral
func (il *IntegerLiteral) expressionNode() {}

// TokenLiteral fulfills the Node interface for IntegerLiteral
func (il *IntegerLiteral) TokenLiteral() string { return il.Token.Literal }

// String fulfills the Node interface for IntegerLiteral
func (il *IntegerLiteral) String() string { return il.Token.Literal }

// PrefixExpression Token.BANG, Token.MINUS
type PrefixExpression struct {
	Token    token.Token
	Operator string
	Right    Expression
}

// expressionNode fulfills the Expression interface for PrefixExpression
func (pe *PrefixExpression) expressionNode() {}

// TokenLiteral fulfills the Node interface for PrefixExpression
func (pe *PrefixExpression) TokenLiteral() string {
	return pe.Token.Literal
}

// String fulfils the Node interface for PrefixExpression
func (pe *PrefixExpression) String() string {
	var out bytes.Buffer

	out.WriteString("(")
	out.WriteString(pe.Operator)
	out.WriteString(pe.Right.String())
	out.WriteString(")")

	return out.String()
}

// InfixExpression Token.PLUS, Token.EQ, ...
type InfixExpression struct {
	Token    token.Token
	Left     Expression
	Operator string
	Right    Expression
}

// expressionNode fulfills the Expression interface for InfixExpression
func (oe *InfixExpression) expressionNode() {}

// TokenLiteral fulfills the Node interface for InfixExpression
func (oe *InfixExpression) TokenLiteral() string {
	return oe.Token.Literal
}

// String fulfils the Node interface for InfixExpression
func (oe *InfixExpression) String() string {
	var out bytes.Buffer

	out.WriteString("(")
	out.WriteString(oe.Left.String())
	out.WriteString(" " + oe.Operator + " ")
	out.WriteString(oe.Right.String())
	out.WriteString(")")

	return out.String()
}

// Boolean literals
type Boolean struct {
	Token token.Token
	Value bool
}

// expressionNode fulfills the Expression interface for Boolean
func (b *Boolean) expressionNode() {}

// TokenLiteral fulfills the Node interface for Boolean
func (b *Boolean) TokenLiteral() string {
	return b.Token.Literal
}

// String fulfils the Node interface for Boolean
func (b *Boolean) String() string {
	return b.Token.Literal
}

// IfExpression Token.IF, Token.ELSE
type IfExpression struct {
	Token       token.Token
	Condition   Expression
	Consequence *BlockStatement
	Alternative *BlockStatement
}

// expressionNode fulfills the Expression interface for IfExpression
func (ie *IfExpression) expressionNode() {}

// TokenLiteral fulfills the Node interface for IfExpression
func (ie *IfExpression) TokenLiteral() string {
	return ie.Token.Literal
}

// String fulfils the Node interface for IfExpression
func (ie *IfExpression) String() string {
	var out bytes.Buffer

	out.WriteString("if")
	out.WriteString(ie.Condition.String())
	out.WriteString(" ")
	out.WriteString(ie.Consequence.String())

	if ie.Alternative != nil {
		out.WriteString("else ")
		out.WriteString(ie.Alternative.String())
	}

	return out.String()
}

// BlockStatement is a block of statements
type BlockStatement struct {
	Token      token.Token
	Statements []Statement
}

// statementNode fulfills the Statement interface for BlockStatement
func (bs *BlockStatement) statementNode() {}

// TokenLiteral fulfills the Node interface for BlockStatement
func (bs *BlockStatement) TokenLiteral() string {
	return bs.Token.Literal
}

// String fulfills the Node interface for BlockStatement
func (bs *BlockStatement) String() string {
	var out bytes.Buffer

	for _, s := range bs.Statements {
		out.WriteString(s.String())
	}

	return out.String()
}

// FunctionLiteral sometimes called an anonymous function
type FunctionLiteral struct {
	Token      token.Token
	Parameters []*Identifier
	Body       *BlockStatement
}

// expressionNode fulfills the Expression interface for FunctionLiteral
func (fl *FunctionLiteral) expressionNode() {}

// TokenLiteral fulfills the Node interface for FunctionLiteral
func (fl *FunctionLiteral) TokenLiteral() string {
	return fl.Token.Literal
}

// String fulfills the Node interface for FunctionLiteral
func (fl *FunctionLiteral) String() string {
	var out bytes.Buffer

	params := []string{}
	for _, p := range fl.Parameters {
		params = append(params, p.String())
	}

	out.WriteString(fl.TokenLiteral())
	out.WriteString("(")
	out.WriteString(strings.Join(params, ", "))
	out.WriteString(")")
	out.WriteString(fl.Body.String())

	return out.String()
}

// CallExpression is a function call
type CallExpression struct {
	Token     token.Token
	Function  Expression
	Arguments []Expression
}

// expressionNode fulfills the Expression interface for CallExpression
func (ce *CallExpression) expressionNode() {}

// TokenLiteral fulfilles the Node interface for CallExpression
func (ce *CallExpression) TokenLiteral() string {
	return ce.Token.Literal
}

// String fulfills the Node interface for CallExpression
func (ce *CallExpression) String() string {
	var out bytes.Buffer

	args := []string{}
	for _, a := range ce.Arguments {
		args = append(args, a.String())
	}

	out.WriteString(ce.Function.String())
	out.WriteString("(")
	out.WriteString(strings.Join(args, ", "))
	out.WriteString(")")

	return out.String()
}

// StringLiteral makes strings first-class citizens in Monkey
type StringLiteral struct {
	Token token.Token
	Value string
}

// expressionNode fulfills Expression interface for StringLiteral
func (sl *StringLiteral) expressionNode() {}

// TokenLiteral fulfills Node interface for StringLiteral
func (sl *StringLiteral) TokenLiteral() string {
	return sl.Token.Literal
}

// String fulfills the Node interface for StringLiteral
func (sl *StringLiteral) String() string {
	return sl.Token.Literal
}

// ArrayLiteral makes arrays first-class citizens in Monkey
type ArrayLiteral struct {
	Token    token.Token
	Elements []Expression
}

// expressionNode fulfills the Expression interface for ArrayLiteral
func (al *ArrayLiteral) expressionNode() {}

// TokenLiteral fulfills the Node interface for ArrayLiteral
func (al *ArrayLiteral) TokenLiteral() string {
	return al.Token.Literal
}

// String fulfills the Node interface for ArrayLiteral
func (al *ArrayLiteral) String() string {
	var out bytes.Buffer

	elements := []string{}
	for _, el := range al.Elements {
		elements = append(elements, el.String())
	}

	out.WriteString("[")
	out.WriteString(strings.Join(elements, ", "))
	out.WriteString("]")

	return out.String()
}

// IndexExpression <expression>[<expression>]
type IndexExpression struct {
	Token token.Token
	Left  Expression
	Index Expression
}

// expressionNode fulfills the Expression interface for IndexExpression
func (ie *IndexExpression) expressionNode() {}

// TokenLiteral fulfills the Node interface for IndexExpression
func (ie *IndexExpression) TokenLiteral() string {
	return ie.Token.Literal
}

// String fulfills the Node interface for IndexExpression
func (ie *IndexExpression) String() string {
	var out bytes.Buffer

	out.WriteString("(")
	out.WriteString(ie.Left.String())
	out.WriteString("[")
	out.WriteString(ie.Index.String())
	out.WriteString("])")

	return out.String()
}

// HashLiteral {<expression> : <expression>, <expression> : <expression>, ...}
type HashLiteral struct {
	Token token.Token
	Pairs map[Expression]Expression
}

// expressionNode fulfills the Expression interface for HashLiteral
func (hl *HashLiteral) expressionNode() {}

// TokenLiteral fulfills the Node interface for HashLiteral
func (hl *HashLiteral) TokenLiteral() string {
	return hl.Token.Literal
}

// String fulfills the Node interface for HashLiteral
func (hl *HashLiteral) String() string {
	var out bytes.Buffer

	pairs := []string{}
	for key, value := range hl.Pairs {
		pairs = append(pairs, key.String()+":"+value.String())
	}

	out.WriteString("{")
	out.WriteString(strings.Join(pairs, ", "))
	out.WriteString("}")

	return out.String()
}
