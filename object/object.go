package object

import (
	"bytes"
	"fmt"
	"hash/fnv"
	"strings"

	"gitlab.com/mark.west1/go-interpreter/ast"
)

// ObjectTypes
const (
	INTEGER_OBJ      = "INTEGER"
	BOOLEAN_OBJ      = "BOOLEAN"
	NULL_OBJ         = "NULL"
	RETURN_VALUE_OBJ = "RETURN_VALUE"
	ERROR_OBJ        = "ERROR"
	FUNCTION_OBJ     = "FUNCTION"
	STRING_OBJ       = "STRING"
	BUILTIN_OBJ      = "BUILTIN"
	ARRAY_OBJ        = "ARRAY"
	HASH_OBJ         = "HASH"
)

// ObjectType is the progenetor of all Monkey objects
type ObjectType string

// Object interface implemented by type objects
type Object interface {
	Type() ObjectType
	Inspect() string
}

// Integer wraps integer values
type Integer struct {
	Value int64
}

// Inspect fulfills the Object interface for Integer
func (i *Integer) Inspect() string {
	return fmt.Sprintf("%d", i.Value)
}

// Type fulfills the Object interface for Integer
func (i *Integer) Type() ObjectType {
	return INTEGER_OBJ
}

// Boolean wraps boolean values
type Boolean struct {
	Value bool
}

// Inspect fulfills the Object interface for Boolean
func (b *Boolean) Inspect() string {
	return fmt.Sprintf("%t", b.Value)
}

// Type fulfills the Object interface for Boolean
func (b *Boolean) Type() ObjectType {
	return BOOLEAN_OBJ
}

// Null wraps null values
type Null struct{}

// Inspect fulfills the Object interface for Null
func (n *Null) Inspect() string {
	return "null"
}

// Type fulfills the Object interface for Null
func (n *Null) Type() ObjectType {
	return NULL_OBJ
}

// ReturnValue encapsulates a return value
type ReturnValue struct {
	Value Object
}

// Type fulfills the Object interface for ReturnValue
func (rv *ReturnValue) Type() ObjectType {
	return RETURN_VALUE_OBJ
}

// Inspect fulfills the Object interface for ReturnValue
func (rv *ReturnValue) Inspect() string {
	return rv.Value.Inspect()
}

// Error encapsulates an evaluation error
type Error struct {
	Message string
}

// Type fulfills the Object interface for Error
func (e *Error) Type() ObjectType {
	return ERROR_OBJ
}

// Inspect fulfills the Object interface for Error
func (e *Error) Inspect() string {
	return "ERROR: " + e.Message
}

// NewEnvironment creates a new environment
func NewEnvironment() *Environment {
	s := make(map[string]Object)
	return &Environment{store: s}
}

// Environment used during evaluation for value binding
type Environment struct {
	store map[string]Object
	outer *Environment
}

// Get retrieves an Object from the Environment and a boolean success/fail
func (e *Environment) Get(name string) (Object, bool) {
	obj, ok := e.store[name]
	if !ok && e.outer != nil {
		obj, ok = e.outer.Get(name)
	}
	return obj, ok
}

// Set sets an Object in the Environment
func (e *Environment) Set(name string, val Object) Object {
	e.store[name] = val
	return val
}

// Function used to evaluate functions
type Function struct {
	Parameters []*ast.Identifier
	Body       *ast.BlockStatement
	Env        *Environment
}

// Type fulfills the Object interface for Function
func (f *Function) Type() ObjectType {
	return FUNCTION_OBJ
}

// Inspect fulfills the Object interface for Function
func (f *Function) Inspect() string {
	var out bytes.Buffer

	params := []string{}
	for _, p := range f.Parameters {
		params = append(params, p.String())
	}

	out.WriteString("fn")
	out.WriteString("(")
	out.WriteString(strings.Join(params, ", "))
	out.WriteString(") {\n")
	out.WriteString(f.Body.String())
	out.WriteString("\n}")

	return out.String()
}

// String for evaluating StringLiteral expressions
type String struct {
	Value string
}

// Type fulfills the Object interface for String
func (s *String) Type() ObjectType { return STRING_OBJ }

// Inspect fulfills the object interface for String
func (s *String) Inspect() string { return s.Value }

// NewEnclosedEnvironment wraps an old environment within a new one
// to preserve variable scope
func NewEnclosedEnvironment(outer *Environment) *Environment {
	env := NewEnvironment()
	env.outer = outer
	return env
}

// BuiltinFunction provided by the Monkey interpreter
type BuiltinFunction func(args ...Object) Object

// Builtin wraps a BuiltinFunction
type Builtin struct {
	Fn BuiltinFunction
}

// Type fulfills the Object interface for Builtin
func (b *Builtin) Type() ObjectType { return BUILTIN_OBJ }

// Inspect fulfills the Object interface for Builtin
func (b *Builtin) Inspect() string { return "builtin function" }

// Array gives first-class status to arrays in Monkey
type Array struct {
	Elements []Object
}

// Type fulfills the Object interface for Array
func (ao *Array) Type() ObjectType { return ARRAY_OBJ }

// Inspect fulfills the Object interface for Array
func (ao *Array) Inspect() string {
	var out bytes.Buffer

	elements := []string{}
	for _, e := range ao.Elements {
		elements = append(elements, e.Inspect())
	}

	out.WriteString("[")
	out.WriteString(strings.Join(elements, ", "))
	out.WriteString("]")

	return out.String()
}

// HashKey allows different objects with the same value to identified
type HashKey struct {
	Type  ObjectType
	Value uint64
}

// HashKey on *Boolean allows Boolean to be a key in a Hash
func (b *Boolean) HashKey() HashKey {
	var value uint64

	if b.Value {
		value = 1
	} else {
		value = 0
	}

	return HashKey{Type: b.Type(), Value: value}
}

// HashKey on *Integer allows Integer to be a key in a Hash
func (i *Integer) HashKey() HashKey {
	return HashKey{Type: i.Type(), Value: uint64(i.Value)}
}

// HashKey on *String allows String to be a key in a Hash
func (s *String) HashKey() HashKey {
	h := fnv.New64a()
	h.Write([]byte(s.Value))

	return HashKey{Type: s.Type(), Value: h.Sum64()}
}

// HashPair required to implement Hash object
type HashPair struct {
	Key   Object
	Value Object
}

// Hash object.HASH
type Hash struct {
	Pairs map[HashKey]HashPair
}

// Type fulfills the Object interface for Hash
func (h *Hash) Type() ObjectType { return HASH_OBJ }

// Inspect fulfills the Object interface for Hash
func (h *Hash) Inspect() string {
	var out bytes.Buffer

	pairs := []string{}
	for _, pair := range h.Pairs {
		pairs = append(pairs, fmt.Sprintf("%s: %s",
			pair.Key.Inspect(), pair.Value.Inspect()))
	}

	out.WriteString("{")
	out.WriteString(strings.Join(pairs, ", "))
	out.WriteString("}")

	return out.String()
}

// Hashable identifies objects on which HashKey() can be called
type Hashable interface {
	HashKey() HashKey
}
